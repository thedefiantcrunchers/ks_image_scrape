#!/usr/bin/env python

import requests
import argparse
import re
from pathlib import Path
from os import sep


def main():
    clean = url.split('?')[0]

    slug = clean.split('/')[4] + '/' + clean.split('/')[5]

    directory = slug.split('/')[0] + sep + slug.split('/')[1]

    Path(directory).mkdir(parents=True, exist_ok=True)


    s = requests.Session()
    r = s.get('https://www.kickstarter.com')

    m = re.search(r'csrf-token\" content=\"(.*)\"', r.text)
    csrf_token = m.group(1)

    data='[{"operationName":"Campaign","variables":{"slug":"'+slug+'"},"query":"query Campaign($slug: String\u0021) {project(slug: $slug) {story(assetWidth: 680)}}"}]'
    r = s.post('https://www.kickstarter.com/graph', headers={
        'content-type': 'application/json',
        'x-csrf-token': csrf_token
        },
        data=data
        )

    m = re.findall(r'(?<=src=\\\")[^\\\"]+', r.text)

    i = 1

    for image in m:
        r = s.get(image.replace('&amp;', '&'))
        f = directory + sep + str(i) + '.' + re.split(r'[/?.]',image)[10].lower()
        open(f, 'wb').write(r.content)
        print(f)
        i += 1

    print("All Done")
    quit()

parser = argparse.ArgumentParser()
parser.add_argument('url', nargs='?', help="URL to parse")

args = parser.parse_args()

if (args.url):
    url = args.url
else:
    url = input("Enter URL: ")

main()
